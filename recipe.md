Broth based lentil soup with fennel, sausage and bitter greens.
- Utilizes a lot of easy to find ingredients from the farmers market
- fast cook-time b/c lentils are pre soaked
- can improvise with whatever meat and veggies you have on hand.

# LENTIL SOUP WITH SAUSAGE, FENNEL & BITTER GREENS
- ETC: 8 HOURS (soaking lentils)
- PREP TIME: 20 MINUTES 
- COOK TIME: 45 MINUTES
- SERVES 4 (~ 2 cups per serving [NOTE](#dec11))

<!--ingredients-->

STEP    | QUANTITY   | INGREDIENT                             | PREPARATION
------  | ---------  | -------------------------------        | ----------------------------------------------------------------------------------
p1 1 3a | 1.5 c      | **lentils** -- italian green           | soaked for 8 hours in a pot of water with 2 tbsp of vinegar or lemon juice
2 3c    | 1 lb       | **sausage** -- italian pork            | casings removed
2       |            | oil or butter                          | for the pan
3       | 1          | **onion** -- large yellow              | finely chopped
3       | 3          | **carrots** -- medium sized            | chopped
3,3e    | 1          | **fennel bulb** [^fennel]              | chopped (reserve a few fronds for garnish)
3       | 4 cloves   | **garlic**                             | finely chopped
3a 3e   | 2          | bay leaves                             |
3a 3e   | 1/2 tsp    | crushed red pepper flakes              |
3a 3e   |            | salt and pepper                        |
3a      | 1/2 cup    | **red wine**                           |
3a      | 7 cups     | **chicken stock** or veggie broth      |
3a      | 1 head     | **curly endive** or escarole [^endive] | torn into bite size pieces
3c      | 1 tsp      | red wine vinegar                       |
3d      |            | **yogurt** -- plain whole milk         | for serving

<!--not sure about using footnotes for this-->
[^fennel]: Fennel lends a **very mild** licorice taste.
- my mom hates licorice and she liked the other fennel soup I made, this was similar.

[^endive]: 
[endive/escarole](https://en.wikipedia.org/wiki/Endive) 
: a genus of bitter leaved vegetibles. Includes:
    - curly endive AKA chicory
    - escarole AKA broad-leafed endive, grumolo, scarola
      - this variety is less bitter
    - leaf chicory AKA common chicory, european chicory

### Pre:
1. Soak lentils for **8 hours** in a pot of water with 2 tbsp of vinegar or lemon juice

### Cook: 
1. Drain and thoroughly rinse the soaked lentils and set them aside.

2. Heat a large **cast iron soup pot or dutch oven** over **medium-high heat**.  
   **Brown the sausage**, using a wooden spoon to break up the meat a bit.  
   Remove from the heat and place on plate or bowl.  

3. Add a little oil or butter to the pan and add the **onion, carrots, fennel and garlic.**   
   Cook, stirring often for about **~5 minutes** or until the veggies are beginning to brown.

    a. Add the spices: **bay leaves, pepper flakes** and a fat pinch of **salt and pepper**, then the **wine, stock and lentils** 
    
    b. **bring to a boil**.  
       Reduce the heat to low and **simmer** the soup for about
       **~35-40 minutes** or until the soup has thickened and the lentils are tender. 
    
    c. Add the **sausage** back to the pan along with the **endive**.   
       Drizzle in the **vinegar** and give the pot a good stir.  
       **Wait for greens to wilt**
       
    d. Divide the soup between bowls with a **dollop with yogurt**.  
      **Garnish** with reserved fennel fronds, add extra extra salt, pepper and crushed red pepper flakes to taste.

***

## Prior Art
Source: <https://dishingupthedirt.com/recipes/lentil-soup-fennel-sausage-bitter-greens/>

## Comments:
[EAS *Mon Dec 11 06:43:53 UTC 2023*]{#dec11}
  - I made the original recipe. Worked out to plenty for 4 guys. 
  - Added whole portebella mushrooms -> good decision.
  - Added dry anise seed from the garden for extra licorice flavor (flavor *still* pretty subtle)

