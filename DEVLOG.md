
Mon Dec 11 05:24:53 UTC 2023
Lets use markdown to write a recipe.

1. Start by making existing recipe render.
2. Then annotate whats wrong with it
3. Make a better version

Pandoc features I'll use
- footnotes [^fn]
- highlight ==for annotations notes==
- strikeout ~~for things that should be removed~~

## formatting the recipe
- pipe-tables for ingredients [^vimalign]
- `+lists_without_preceding_blankline` *non-default extension*

pipe tables allows ommiting the first `|`, but then it seems you *have* to have non-whitespace in the first cell.
In order to avoid this and still be able to vim-easy-align, we will include the first `|`

[^fn]: for out-of-text annotations

[^vimalign]: How to align pipe tables in vim
    https://github.com/junegunn/vim-easy-align
    - `gaip*|` (think "go align in paragraph multiple pipe")
    - `gaip<enter>1|` enter to switch alignment, 1 for first cell
